/// Generates an error type that can be used with `error_stack`
#[macro_export]
macro_rules! error {
    ($vis: vis$name: ident$(<$T: ident>)?; $($display: tt)*) => {
        #[allow(missing_docs)]
        $vis struct $name $(<$T>(std::marker::PhantomData<fn() -> $T>))?;

        impl$(<$T>)? std::fmt::Debug for $name$(<$T>)? {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                write!(
                    f,
                    "{}",
                    stringify!($name),
                )$(?;

                write!(f, "{}",
                    std::any::type_name::<$T>()
                )
                )?
            }
        }

        impl$(<$T>)? std::fmt::Display for $name$(<$T>)? {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
                $(
                    #![allow(non_snake_case)]
                   let $T = std::any::type_name::<$T>();
                )?

                write!(f, $($display)*)
            }
        }

        impl$(<$T: 'static>)? error_stack::Context for $name$(<$T>)? {}
    };

    ($vis: vis$name: ident{$($arg_vis: vis $arg_name: ident: $data: ty),*} $($display: tt)*) => {
        #[allow(missing_docs)]
        #[derive(Debug)]
        $vis struct $name {$($arg_vis $arg_name: $data),*}

        impl std::fmt::Display for $name {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
                $(
                    let $arg_name = &self.$arg_name;
                )*

                write!(f, $($display)*)
            }
        }

        impl error_stack::Context for $name {}
    }
}

#[cfg(test)]
mod test {
    error!(pub Bruh; "Oof");
    error!(Oof { a: String } "Pog {a}");
    error!(pub(crate) Generic<T>; "Pog {T}");
}
